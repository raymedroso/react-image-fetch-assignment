import React from 'react'

export default function({togglePlay, isPlaying }) {
    return <button className="btn" onClick={togglePlay}>{isPlaying ? 'PAUSE': 'PLAY'}</button>
}