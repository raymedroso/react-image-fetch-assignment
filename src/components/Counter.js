import React from 'react'

export default function({label, count}) {
    return <h3>
        {label}: {count}
    </h3>
}