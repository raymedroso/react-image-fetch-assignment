import React from 'react';

const imgStyles = {
    width: '640px',
    height: '400px'
}

export default ({ url }) => {
    if (!url) {
        return <div className="img-wrapper">Please wait, loading image...</div>;
     }

    return <div className="img-wrapper"><img src={url} alt={""} style={imgStyles}/></div>;
}