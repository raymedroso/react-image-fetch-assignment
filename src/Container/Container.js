import React from 'react';
import ImageContainer from '../components/ImageContainer';
import Counter from '../components/Counter';
import Button from '../components/Button';

const {REACT_APP_API_URL, REACT_APP_INTERVAL } = process.env
export default class Container extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            url: undefined,
            count: 0,
            streak: 0,
            isPlaying: false
        }
    }

    componentDidMount() {
        this.fetchData();
    }
    
    fetchData = () => {
        setInterval(async () => {
            let { url: stateUrl, streak, count, isPlaying } = this.state;
            if(isPlaying) {
                try {
                    const { url } = await fetch(REACT_APP_API_URL);
                    
                    if(url === stateUrl) 
                        ++streak;
                    else
                        streak = 0;
    
                    this.setState({ url, streak, count: count + 1 });
                } catch(error) {
                    console.log(error);
                }
            }
        }, REACT_APP_INTERVAL);
    };

    togglePlay = () => this.setState(({isPlaying}) => ({isPlaying: !isPlaying}));

    render(){
        const { url, count, streak, isPlaying } = this.state;

        return (
            <div className="main-wrapper">
                <div className="count-container">
                    <Counter label="Counter" count={count} />
                    {streak > 0 && <Counter label="Streak" count={streak} />}
                </div>
                <ImageContainer url={url}/>
                <Button isPlaying={isPlaying} togglePlay={this.togglePlay}/>
            </div>
        )
    }

}